﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckMQ
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void CheckButtonClick(object sender, EventArgs e)
        {
            var trustStoreFilePath = this.ctlTrustStoreFile.Text;
            if (String.IsNullOrEmpty(trustStoreFilePath) == false &&
                File.Exists(trustStoreFilePath) == false)
            {
                MessageBox.Show(this, String.Format("File [{0}] not found", trustStoreFilePath));
                return;
            }

            this.ctlInputPanel.Enabled = false;
            this.ctlOutputText.Clear();
            this.ctlErrorText.Clear();

            var jvmExePath = Path.Combine(Program.Directory, "jdk", "bin", "javaw.exe");
            var javaDataDir = Path.Combine(Program.Directory, "cli");
            var javaSrcFile = Path.GetFileName(
                Directory.EnumerateFiles(javaDataDir, "*.java").Single()
            );
            var classPathList = new List<string> { "." };
            classPathList.AddRange(
                Directory.EnumerateFiles(javaDataDir, "*.jar").Select(Path.GetFileName)
            );

            var processArgs = new StringBuilder();
            if (this.ctlDebugSSL.Checked)
            {
                processArgs.Append("-Djavax.net.debug=ssl").Append(' ');
            }
            processArgs.Append("-Dfile.encoding=UTF-8 -classpath ").Append('"')
                .Append(String.Join(";", classPathList)).Append('"')
                .Append(' ').Append(javaSrcFile);

            var processInfo = new ProcessStartInfo(jvmExePath, processArgs.ToString())
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                WorkingDirectory = javaDataDir
            }.AddEnvVar("CHECK_TIMEOUT_MS", this.ctlCheckProcessTimeout.Value.ToString())
            .AddEnvVar("TCP_URI", this.ctlOpenwireText.Text)
            .AddEnvVar("WS_URI", this.ctlStompText.Text)
            .AddEnvVar("TOPIC_NAME", this.ctlTopicText.Text)
            .AddEnvVar("BROKER_USERNAME", this.ctlBrokerUser.Text)
            .AddEnvVar("BROKER_PASSCODE", this.ctlBrokerPass.Text)
            .AddEnvVar("TRUST_KEYSTORE", trustStoreFilePath)
            .AddEnvVar("KEYSTORE_PASSWORD", this.ctlTrustStorePass.Text);

            this.StartCheckProcess(processInfo);
        }

        private void StartCheckProcess(ProcessStartInfo info)
        {
            this.mCheckProcess = new Process
            {
                StartInfo = info,
                EnableRaisingEvents = true
            };
            this.mCheckProcess.Exited += this.CheckProcessExited;
            this.mCheckProcess.OutputDataReceived += this.CheckProcessOutputDataHandler;
            this.mCheckProcess.ErrorDataReceived += this.CheckProcessErrorDataHandler;

            try
            {
                this.mCheckProcess.Start();
                this.mCheckProcess.BeginOutputReadLine();
                this.mCheckProcess.BeginErrorReadLine();
            }
            catch (Exception ex)
            {
                this.ctlErrorText.Text += String.Format(
                    "Exception Message: {0}{1}",
                    ex.Message,
                    Environment.NewLine);
                this.ctlErrorText.Text += String.Format(
                    "Exception Type: {0}{1}",
                    ex.GetType().FullName,
                    Environment.NewLine);
                this.ctlErrorText.Text += String.Format(
                    "Exception StackTrace: {0}{1}",
                    ex.StackTrace,
                    Environment.NewLine);

                this.DestroyCheckProcess();
                this.ctlInputPanel.Enabled = true;
            }
        }

        private void DestroyCheckProcess()
        {
            if (this.mCheckProcess == null) return;

            try { this.mCheckProcess.Dispose(); }
            catch { }
            finally { this.mCheckProcess = null; }
        }

        private void CheckProcessOutputDataHandler(object sender, DataReceivedEventArgs e)
        {
            var text = (e.Data ?? String.Empty).Trim();
            if (String.IsNullOrEmpty(text)) return;

            var textBox = this.ctlOutputText;
            Action appendText = () => { textBox.Text += Environment.NewLine + text; };

            if (textBox.InvokeRequired)
            {
                textBox.Invoke(appendText);
            }
            else
            {
                appendText();
            }
        }

        private void CheckProcessErrorDataHandler(object sender, DataReceivedEventArgs e)
        {
            var text = (e.Data ?? String.Empty).Trim();
            if (String.IsNullOrEmpty(text)) return;

            var textBox = this.ctlErrorText;
            Action appendText = () => { textBox.Text += Environment.NewLine + text; };

            if (textBox.InvokeRequired)
            {
                textBox.Invoke(appendText);
            }
            else
            {
                appendText();
            }
        }

        private void CheckProcessExited(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(this.CheckProcessExitedHandler));
            }
            else
            {
                this.CheckProcessExitedHandler();
            }

            this.DestroyCheckProcess();
        }

        private void CheckProcessExitedHandler()
        {
            this.ctlInputPanel.Enabled = true;

            if (this.mCheckProcess == null)
                return;

            if (this.mCheckProcess.ExitCode == 0)
            {
                MessageBox.Show(this, "ActiveMQ is OK");
            }
            else
            {
                MessageBox.Show(this, "An error occurred when checking MQ");
                this.ctlErrorText.Text += Environment.NewLine;
                this.ctlErrorText.Text += String.Format(
                    "Process exit with code: {0}",
                    this.mCheckProcess.ExitCode);
            }
        }

        private Process mCheckProcess = null;
    }
}
