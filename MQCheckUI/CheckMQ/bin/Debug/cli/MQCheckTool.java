import java.io.DataInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.WebSocket;
import java.net.http.WebSocket.Listener;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.apache.activemq.transport.stomp.Stomp;
import org.apache.activemq.transport.stomp.Stomp.Headers;
import org.apache.activemq.transport.stomp.StompFrame;
import org.apache.activemq.transport.stomp.StompFrameError;
import org.apache.activemq.transport.stomp.StompWireFormat;
import org.apache.commons.io.input.CharSequenceInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class MQCheckTool {

    private static final Logger LOG = LoggerFactory.getLogger(MQCheckTool.class);
    
    private static final String WSS_URI = "wss://localhost:61623";
    private static final String TCP_URI = "tcp://localhost:61616";
    
    private static final String TOPIC_NAME = "Wechat";
    private static final String BROKER_USERNAME = "pony";
    private static final String BROKER_PASSCODE = "mht123";
    private static final String KEYSTORE_TYPE = "jks";
    private static final String KEYSTORE_PASSWORD = "password";
    private static final Path TRUST_KEYSTORE = Paths.get(".", "client.keystore");
    
    private static final String CHECK_TIMEOUT_MS = "10000";
    
    public static void main(String[] args) {
        
        Map<String, String> envMap = System.getenv();
        dumpEnvInfo(envMap);
        
        var websocketUri = envMap.getOrDefault("WS_URI", WSS_URI);
        if (websocketUri.startsWith("wss:")) {
            System.setProperty("javax.net.ssl.trustStore",
                envMap.getOrDefault("TRUST_KEYSTORE",
                    TRUST_KEYSTORE.toAbsolutePath().normalize().toString())
            );
            System.setProperty("javax.net.ssl.trustStorePassword",
                envMap.getOrDefault("KEYSTORE_PASSWORD", KEYSTORE_PASSWORD)
            );
            System.setProperty("javax.net.ssl.trustStoreType",
                envMap.getOrDefault("KEYSTORE_TYPE", KEYSTORE_TYPE)
            );
        }

        var success = false;
        var chker = new Checker();
        chker.setWebsocketUri(websocketUri);
        chker.setTcpUri(envMap.getOrDefault("TCP_URI", TCP_URI));
        chker.setTopicName(envMap.getOrDefault("TOPIC_NAME", TOPIC_NAME));
        chker.setBrokerUserName(envMap.getOrDefault("BROKER_USERNAME", BROKER_USERNAME));
        chker.setBrokerPassCode(envMap.getOrDefault("BROKER_PASSCODE", BROKER_PASSCODE));
        
        final var timeout = Long.parseLong(
            envMap.getOrDefault("CHECK_TIMEOUT_MS", CHECK_TIMEOUT_MS));
        new Thread(() -> {
            try {
                Thread.sleep(timeout);
                LOG.error("Check timeout");
            } catch (Throwable e) {
                System.exit(3);
            }
            System.exit(2);
        }).start();
        
        try {
            success = chker.doCheck();
            var error = chker.getStompError();
            if (success == false && error != null) {
                LOG.error(error.getMessage(), error);
            }
        } catch (Throwable e) {
            LOG.error(e.getMessage(), e);
        } finally {
            chker.destroy();
        }
        
        System.exit(success ? 0 : 1);
    }

    private static void dumpEnvInfo(Map<String, String> envMap) {
        if (!LOG.isTraceEnabled()) return;
        
        LOG.trace("Environment variables:");
        for (var pairs : envMap.entrySet()) {
            LOG.trace("{}={}", pairs.getKey(), pairs.getValue());
        }
        
        Optional<String[]> args = ProcessHandle.current().info().arguments();
        args.ifPresent(x -> LOG.trace("Process arguments count:{}", x.length));
        args.map(Arrays::stream).orElse(Stream.empty()).forEach(LOG::trace);
    }
}

final class Checker {

    private static final Logger LOG = LoggerFactory.getLogger(Checker.class);
    
    private String websocketUri;
    
    private String tcpUri;
    
    private String topicName;
    
    private String brokerUserName;
    
    private String brokerPassCode;
    
    private StompWSConnection stompClient;

    public Checker() {
        this.websocketUri = "ws://localhost:61614";
        this.tcpUri = "tcp://localhost:61616";
        this.topicName = "Wechat";
        this.brokerUserName = "pony";
        this.brokerPassCode = "mht123";
    }
    
    public boolean doCheck() throws Exception {
        String subscribeId = UUID.randomUUID().toString();
        URI wsStompUri = URI.create(this.websocketUri);
        LOG.debug("Open WebSocket connection - {}", this.websocketUri);
        
        this.stompClient = new StompWSConnection();
        HttpClient.newHttpClient().newWebSocketBuilder()
            .subprotocols("v12.stomp", "v11.stomp")
            .buildAsync(wsStompUri, this.stompClient);
        this.stompClient.awaitConnection();
        
        var connectReq = new StompFrame(Stomp.Commands.STOMP, Map.of(
            Headers.Connect.ACCEPT_VERSION, String.join(Stomp.COMMA, Stomp.V1_1, Stomp.V1_2),
            Headers.Connect.LOGIN, this.brokerUserName,
            Headers.Connect.PASSCODE, this.brokerPassCode
        ));
        this.stompClient.sendFrame(connectReq);
        
        LOG.debug("Open STOMP connection");
        StompFrame connectRsp = this.stompClient.receive();
        if (!Stomp.Responses.CONNECTED.equals(connectRsp.getAction())) {
            LOG.error("STOMP connect fail");
            return false;
        }
        
        LOG.info("Subscribe topic over STOMP broker");
        var subscribeCmd = new StompFrame(Stomp.Commands.SUBSCRIBE, Map.of(
            Stomp.Headers.Subscribe.ID, subscribeId,
            Stomp.Headers.Subscribe.DESTINATION, this.getStompDestination()
        ));
        this.stompClient.sendFrame(subscribeCmd);
        
        OpenwirePublisher publisher = null;
        StompFrame msgFrame = null; 
        var msgText = String.valueOf(System.currentTimeMillis());
        try {
            LOG.debug("Open TCP connection");
            publisher = new OpenwirePublisher(this.tcpUri, this.topicName);
            publisher.connect(this.brokerUserName, this.brokerPassCode);
            
            LOG.info("Send message over TCP connection");
            publisher.send(msgText);
            
            LOG.info("Receive message from STOMP broker");
            msgFrame = this.stompClient.receive();
        } catch (Exception e) {
            LOG.error("Message send receive fail", e);
            return false;
        } finally {
            publisher.close();
            publisher = null;
        }
        
        var success = false;
        
        if (msgFrame != null &&
            Stomp.Responses.MESSAGE.equals(msgFrame.getAction()) &&
            msgText.equals(msgFrame.getBody())) {
            
            Map<String, String> headers = msgFrame.getHeaders();
            if (this.getStompDestination().equals(
                    headers.get(Stomp.Headers.Message.DESTINATION)) &&
                subscribeId.equals(headers.get(Stomp.Headers.Message.SUBSCRIPTION))) {
                
                LOG.info("Received message confirmed");
                success = true;
            } 
        }
        
        LOG.debug("Unsubscribe topic over STOMP broker");
        var unsubscribeCmd = new StompFrame(Stomp.Commands.UNSUBSCRIBE, Map.of(
            Stomp.Headers.Unsubscribe.ID, subscribeId
        ));
        this.stompClient.sendFrame(unsubscribeCmd);
        
        var disconnectReq = new StompFrame(Stomp.Commands.DISCONNECT, Map.of(
            Stomp.Headers.RECEIPT_REQUESTED, UUID.randomUUID().toString()
        ));
        this.stompClient.sendFrame(disconnectReq);
        
        LOG.debug("Close STOMP connection");
        StompFrame disconnectRsp = this.stompClient.receive();
        if (!Stomp.Responses.RECEIPT.equals(disconnectRsp.getAction())) {
            LOG.error("STOMP disconnect fail");
        }
        
        return success;
    }
    
    public void destroy() {
        if (this.stompClient == null) return;
        
        try {
            this.stompClient.close();
        } catch (Throwable e) {}
    }
    
    public void setWebsocketUri(String websocketUri) {
        this.websocketUri = websocketUri;
    }

    public void setTcpUri(String tcpUri) {
        this.tcpUri = tcpUri;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public void setBrokerUserName(String brokerUserName) {
        this.brokerUserName = brokerUserName;
    }

    public void setBrokerPassCode(String brokerPassCode) {
        this.brokerPassCode = brokerPassCode;
    }
    
    public Throwable getStompError() {
        if (this.stompClient == null) return null;
        
        return this.stompClient.getError();
    }
    
    private String getStompDestination() {
        return "/topic/" + this.topicName;
    }
}

final class OpenwirePublisher {

    private static final Logger LOG = LoggerFactory.getLogger(OpenwirePublisher.class);
    
    private final String brokerUrl;
    private final String topicName;
    
    private Connection mqConnection = null;
    private Session mqSession = null;
    private MessageProducer producer = null;
    
    public OpenwirePublisher(String url, String topic) {
        this.brokerUrl = url;
        this.topicName = topic;
    }
    
    public void connect(String user, String pass) throws JMSException {
        
        var factory = new ActiveMQConnectionFactory(this.brokerUrl);
        this.mqConnection = factory.createConnection(user, pass);
        this.mqConnection.start();
        
        this.mqSession = this.mqConnection
            .createSession(false, Session.AUTO_ACKNOWLEDGE);
        this.producer = this.mqSession
            .createProducer(new ActiveMQTopic(this.topicName));
        this.producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    }
    
    public void send(String msgBody) throws JMSException {
        
        var textMsg = this.mqSession.createTextMessage(msgBody);
        if (LOG.isTraceEnabled()) {
            LOG.trace("Send openwire text message:");
            LOG.trace(msgBody);
        }
        this.producer.send(textMsg);
    }
    
    public void close() {
        try {
            if (this.mqConnection != null) {
                this.mqConnection.stop();
                this.mqConnection.close();
            }
        } catch(Exception ex) {
            if (LOG.isTraceEnabled())
                LOG.trace("Openwire close with error:", ex);
        } finally {
            this.producer = null;
            this.mqSession = null;
            this.mqConnection = null;
        }
    }
}

class StompWSConnection implements Listener {

    private static final Logger LOG = LoggerFactory.getLogger(StompWSConnection.class);
    
    private final StompWireFormat wireFormat = new StompWireFormat();
    private final CountDownLatch connectLatch = new CountDownLatch(1);
    private final BlockingQueue<StompFrame> prefetch = new LinkedBlockingDeque<StompFrame>();
    
    private WebSocket connection = null;
    private Integer closeCode = null;
    private String closeMessage = null;
    private Throwable wsError = null;
    
    public void awaitConnection() throws InterruptedException {
        this.connectLatch.await();
    }

    public boolean awaitConnection(long time, TimeUnit unit) throws InterruptedException {
        return this.connectLatch.await(time, unit);
    }
    
    public StompFrame receive() throws Exception {
        this.checkConnected();
        return this.prefetch.take();
    }

    public StompFrame receive(long timeout, TimeUnit unit) throws Exception {
        this.checkConnected();
        return this.prefetch.poll(timeout, unit);
    }

    public StompFrame receiveNoWait() throws Exception {
        this.checkConnected();
        return this.prefetch.poll();
    }
    
    public synchronized void sendRawFrame(String rawFrame) throws Exception {
        this.checkConnected();
        
        if (LOG.isTraceEnabled()) {
            LOG.trace("Send raw STOMP Frame:");
            LOG.trace(rawFrame);
        }
        this.connection.sendText(rawFrame, true);
    }
    
    public synchronized void sendFrame(StompFrame frame) throws Exception {
        this.checkConnected();
        
        var frameString = this.wireFormat.marshalToString(frame);
        if (LOG.isTraceEnabled()) {
            LOG.trace("Send marshal STOMP frame:");
            LOG.trace(frameString);
        }
        this.connection.sendText(frameString, true);
    }
    
    public synchronized void keepAlive() throws Exception {
        this.checkConnected();
        this.connection.sendText("\n", true);
    }
    
    public void close() {
        if (this.connection != null) {
            this.connection.abort();
            this.connection = null;
        }
    }
    
    @Override
    public void onOpen(WebSocket webSocket) {
        LOG.trace("STOMP WS connection open");
        
        this.connection = webSocket;
        this.connectLatch.countDown();
        
        webSocket.request(1);
    }

    @Override
    public CompletionStage<?> onText(WebSocket webSocket, CharSequence data, boolean last) {
        
        webSocket.request(1);
        if (data == null || data.length() == 0) return null;
        
        if (data.length() == 1 && data.charAt(0) == '\n') {
            LOG.trace("New incoming heartbeat read");
        } else {
            if (LOG.isTraceEnabled()) {
                LOG.trace("New incoming STOMP frame read:");
                LOG.trace(data.toString());
            }

            try {
                var frame = (StompFrame)this.wireFormat.unmarshal(
                    new DataInputStream(
                        new CharSequenceInputStream(
                            data, StandardCharsets.UTF_8)));
                
                if (frame instanceof StompFrameError) {
                    this.wsError = ((StompFrameError)frame).getException();
                    this.close();
                } else {
                    this.prefetch.add(frame);
                }
            } catch (Exception e) {
                this.wsError = e;
                this.close();
            }
        }
        return null;
    }

    @Override
    public CompletionStage<?> onClose(WebSocket webSocket, int statusCode, String reason) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("STOMP WS connection closed - code:{} - reason:{}", statusCode, reason);
        }
        
        this.connection = null;
        this.closeCode = statusCode;
        this.closeMessage = reason;
        
        return null;
    }

    @Override
    public void onError(WebSocket webSocket, Throwable error) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("STOMP WS connection closed with error:", error);
        }
        
        this.connection = null;
        this.wsError = error;
    }
    
    public Integer getCloseCode() {
        return this.closeCode;
    }

    public String getCloseMessage() {
        return this.closeMessage;
    }
    
    public WebSocket getConnection() {
        return this.connection;
    }
    
    public Throwable getError() {
        return this.wsError;
    }
    
    public boolean isConnected() {
        if (this.connection == null || this.wsError != null ||
            this.connection.isInputClosed() ||
            this.connection.isOutputClosed()) {
            
            return false;
        }
        return true;
    }
    
    private void checkConnected() throws IOException {
        if (!this.isConnected()) {
            this.close();
            if (this.wsError != null) {
                throw new IOException("STOMP WS connection is closed with error", this.wsError);
            } else {
                throw new IOException("STOMP WS connection is closed");
            }
        }
    }
}
