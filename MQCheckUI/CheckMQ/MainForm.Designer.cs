﻿namespace CheckMQ
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.ctlTablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.ctlInputPanel = new System.Windows.Forms.Panel();
            this.ctlCheckButton = new System.Windows.Forms.Button();
            this.lblOpenwireUri = new System.Windows.Forms.Label();
            this.ctlOpenwireText = new System.Windows.Forms.TextBox();
            this.lblStompUri = new System.Windows.Forms.Label();
            this.ctlStompText = new System.Windows.Forms.TextBox();
            this.lblTopicName = new System.Windows.Forms.Label();
            this.ctlTopicText = new System.Windows.Forms.TextBox();
            this.lblBrokerUserName = new System.Windows.Forms.Label();
            this.ctlBrokerUser = new System.Windows.Forms.TextBox();
            this.lblBrokerPasscode = new System.Windows.Forms.Label();
            this.ctlBrokerPass = new System.Windows.Forms.TextBox();
            this.lblTrustStorePassword = new System.Windows.Forms.Label();
            this.ctlTrustStorePass = new System.Windows.Forms.TextBox();
            this.lblTrustStoreFullPath = new System.Windows.Forms.Label();
            this.ctlTrustStoreFile = new System.Windows.Forms.TextBox();
            this.lblCheckProcessTimeout = new System.Windows.Forms.Label();
            this.ctlCheckProcessTimeout = new System.Windows.Forms.NumericUpDown();
            this.lblProcessTimeoutUnit = new System.Windows.Forms.Label();
            this.ctlDebugSSL = new System.Windows.Forms.CheckBox();
            this.grpProcessOutput = new System.Windows.Forms.GroupBox();
            this.ctlOutputText = new System.Windows.Forms.TextBox();
            this.grpProcessError = new System.Windows.Forms.GroupBox();
            this.ctlErrorText = new System.Windows.Forms.TextBox();
            this.ctlTablePanel.SuspendLayout();
            this.ctlInputPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlCheckProcessTimeout)).BeginInit();
            this.grpProcessOutput.SuspendLayout();
            this.grpProcessError.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctlTablePanel
            // 
            this.ctlTablePanel.ColumnCount = 1;
            this.ctlTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ctlTablePanel.Controls.Add(this.ctlInputPanel, 0, 0);
            this.ctlTablePanel.Controls.Add(this.grpProcessOutput, 0, 1);
            this.ctlTablePanel.Controls.Add(this.grpProcessError, 0, 2);
            this.ctlTablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlTablePanel.Location = new System.Drawing.Point(0, 0);
            this.ctlTablePanel.Name = "ctlTablePanel";
            this.ctlTablePanel.RowCount = 3;
            this.ctlTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.ctlTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ctlTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ctlTablePanel.Size = new System.Drawing.Size(484, 461);
            this.ctlTablePanel.TabIndex = 0;
            // 
            // ctlInputPanel
            // 
            this.ctlInputPanel.Controls.Add(this.ctlCheckButton);
            this.ctlInputPanel.Controls.Add(this.lblOpenwireUri);
            this.ctlInputPanel.Controls.Add(this.ctlOpenwireText);
            this.ctlInputPanel.Controls.Add(this.lblStompUri);
            this.ctlInputPanel.Controls.Add(this.ctlStompText);
            this.ctlInputPanel.Controls.Add(this.lblTopicName);
            this.ctlInputPanel.Controls.Add(this.ctlTopicText);
            this.ctlInputPanel.Controls.Add(this.lblBrokerUserName);
            this.ctlInputPanel.Controls.Add(this.ctlBrokerUser);
            this.ctlInputPanel.Controls.Add(this.lblBrokerPasscode);
            this.ctlInputPanel.Controls.Add(this.ctlBrokerPass);
            this.ctlInputPanel.Controls.Add(this.lblTrustStorePassword);
            this.ctlInputPanel.Controls.Add(this.ctlTrustStorePass);
            this.ctlInputPanel.Controls.Add(this.lblTrustStoreFullPath);
            this.ctlInputPanel.Controls.Add(this.ctlTrustStoreFile);
            this.ctlInputPanel.Controls.Add(this.lblCheckProcessTimeout);
            this.ctlInputPanel.Controls.Add(this.ctlCheckProcessTimeout);
            this.ctlInputPanel.Controls.Add(this.lblProcessTimeoutUnit);
            this.ctlInputPanel.Controls.Add(this.ctlDebugSSL);
            this.ctlInputPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlInputPanel.Location = new System.Drawing.Point(3, 3);
            this.ctlInputPanel.Name = "ctlInputPanel";
            this.ctlInputPanel.Padding = new System.Windows.Forms.Padding(20);
            this.ctlInputPanel.Size = new System.Drawing.Size(478, 209);
            this.ctlInputPanel.TabIndex = 0;
            // 
            // ctlCheckButton
            // 
            this.ctlCheckButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlCheckButton.Location = new System.Drawing.Point(362, 23);
            this.ctlCheckButton.Name = "ctlCheckButton";
            this.ctlCheckButton.Size = new System.Drawing.Size(93, 63);
            this.ctlCheckButton.TabIndex = 17;
            this.ctlCheckButton.Text = "Check";
            this.ctlCheckButton.UseVisualStyleBackColor = true;
            this.ctlCheckButton.Click += new System.EventHandler(this.CheckButtonClick);
            // 
            // lblOpenwireUri
            // 
            this.lblOpenwireUri.AutoSize = true;
            this.lblOpenwireUri.Location = new System.Drawing.Point(20, 23);
            this.lblOpenwireUri.Name = "lblOpenwireUri";
            this.lblOpenwireUri.Size = new System.Drawing.Size(83, 12);
            this.lblOpenwireUri.TabIndex = 0;
            this.lblOpenwireUri.Text = "OpenWire URI:";
            // 
            // ctlOpenwireText
            // 
            this.ctlOpenwireText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlOpenwireText.Location = new System.Drawing.Point(109, 17);
            this.ctlOpenwireText.Name = "ctlOpenwireText";
            this.ctlOpenwireText.Size = new System.Drawing.Size(236, 21);
            this.ctlOpenwireText.TabIndex = 1;
            this.ctlOpenwireText.Text = "tcp://localhost:61616";
            // 
            // lblStompUri
            // 
            this.lblStompUri.AutoSize = true;
            this.lblStompUri.Location = new System.Drawing.Point(38, 48);
            this.lblStompUri.Name = "lblStompUri";
            this.lblStompUri.Size = new System.Drawing.Size(65, 12);
            this.lblStompUri.TabIndex = 3;
            this.lblStompUri.Text = "STOMP URI:";
            // 
            // ctlStompText
            // 
            this.ctlStompText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlStompText.Location = new System.Drawing.Point(109, 44);
            this.ctlStompText.Name = "ctlStompText";
            this.ctlStompText.Size = new System.Drawing.Size(236, 21);
            this.ctlStompText.TabIndex = 2;
            this.ctlStompText.Text = "wss://localhost:61623";
            // 
            // lblTopicName
            // 
            this.lblTopicName.AutoSize = true;
            this.lblTopicName.Location = new System.Drawing.Point(32, 74);
            this.lblTopicName.Name = "lblTopicName";
            this.lblTopicName.Size = new System.Drawing.Size(71, 12);
            this.lblTopicName.TabIndex = 5;
            this.lblTopicName.Text = "Topic Name:";
            // 
            // ctlTopicText
            // 
            this.ctlTopicText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlTopicText.Location = new System.Drawing.Point(109, 71);
            this.ctlTopicText.Name = "ctlTopicText";
            this.ctlTopicText.Size = new System.Drawing.Size(236, 21);
            this.ctlTopicText.TabIndex = 4;
            this.ctlTopicText.Text = "Wechat";
            // 
            // lblBrokerUserName
            // 
            this.lblBrokerUserName.AutoSize = true;
            this.lblBrokerUserName.Location = new System.Drawing.Point(23, 101);
            this.lblBrokerUserName.Name = "lblBrokerUserName";
            this.lblBrokerUserName.Size = new System.Drawing.Size(107, 12);
            this.lblBrokerUserName.TabIndex = 7;
            this.lblBrokerUserName.Text = "Broker User Name:";
            // 
            // ctlBrokerUser
            // 
            this.ctlBrokerUser.Location = new System.Drawing.Point(136, 98);
            this.ctlBrokerUser.Name = "ctlBrokerUser";
            this.ctlBrokerUser.Size = new System.Drawing.Size(90, 21);
            this.ctlBrokerUser.TabIndex = 6;
            this.ctlBrokerUser.Text = "pony";
            // 
            // lblBrokerPasscode
            // 
            this.lblBrokerPasscode.AutoSize = true;
            this.lblBrokerPasscode.Location = new System.Drawing.Point(232, 101);
            this.lblBrokerPasscode.Name = "lblBrokerPasscode";
            this.lblBrokerPasscode.Size = new System.Drawing.Size(101, 12);
            this.lblBrokerPasscode.TabIndex = 9;
            this.lblBrokerPasscode.Text = "Broker Password:";
            // 
            // ctlBrokerPass
            // 
            this.ctlBrokerPass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlBrokerPass.Location = new System.Drawing.Point(343, 98);
            this.ctlBrokerPass.Name = "ctlBrokerPass";
            this.ctlBrokerPass.Size = new System.Drawing.Size(112, 21);
            this.ctlBrokerPass.TabIndex = 8;
            this.ctlBrokerPass.Text = "mht123";
            this.ctlBrokerPass.UseSystemPasswordChar = true;
            // 
            // lblTrustStorePassword
            // 
            this.lblTrustStorePassword.AutoSize = true;
            this.lblTrustStorePassword.Location = new System.Drawing.Point(23, 155);
            this.lblTrustStorePassword.Name = "lblTrustStorePassword";
            this.lblTrustStorePassword.Size = new System.Drawing.Size(125, 12);
            this.lblTrustStorePassword.TabIndex = 11;
            this.lblTrustStorePassword.Text = "TrustStore Password:";
            // 
            // ctlTrustStorePass
            // 
            this.ctlTrustStorePass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlTrustStorePass.Location = new System.Drawing.Point(160, 152);
            this.ctlTrustStorePass.Name = "ctlTrustStorePass";
            this.ctlTrustStorePass.Size = new System.Drawing.Size(295, 21);
            this.ctlTrustStorePass.TabIndex = 10;
            this.ctlTrustStorePass.UseSystemPasswordChar = true;
            // 
            // lblTrustStoreFullPath
            // 
            this.lblTrustStoreFullPath.AutoSize = true;
            this.lblTrustStoreFullPath.Location = new System.Drawing.Point(23, 128);
            this.lblTrustStoreFullPath.Name = "lblTrustStoreFullPath";
            this.lblTrustStoreFullPath.Size = new System.Drawing.Size(131, 12);
            this.lblTrustStoreFullPath.TabIndex = 13;
            this.lblTrustStoreFullPath.Text = "TrustStore File Path:";
            // 
            // ctlTrustStoreFile
            // 
            this.ctlTrustStoreFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlTrustStoreFile.Location = new System.Drawing.Point(160, 125);
            this.ctlTrustStoreFile.Name = "ctlTrustStoreFile";
            this.ctlTrustStoreFile.Size = new System.Drawing.Size(295, 21);
            this.ctlTrustStoreFile.TabIndex = 12;
            // 
            // lblCheckProcessTimeout
            // 
            this.lblCheckProcessTimeout.AutoSize = true;
            this.lblCheckProcessTimeout.Location = new System.Drawing.Point(20, 181);
            this.lblCheckProcessTimeout.Name = "lblCheckProcessTimeout";
            this.lblCheckProcessTimeout.Size = new System.Drawing.Size(137, 12);
            this.lblCheckProcessTimeout.TabIndex = 15;
            this.lblCheckProcessTimeout.Text = "Check Process Timeout:";
            // 
            // ctlCheckProcessTimeout
            // 
            this.ctlCheckProcessTimeout.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.ctlCheckProcessTimeout.Location = new System.Drawing.Point(166, 179);
            this.ctlCheckProcessTimeout.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.ctlCheckProcessTimeout.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.ctlCheckProcessTimeout.Name = "ctlCheckProcessTimeout";
            this.ctlCheckProcessTimeout.Size = new System.Drawing.Size(82, 21);
            this.ctlCheckProcessTimeout.TabIndex = 14;
            this.ctlCheckProcessTimeout.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // lblProcessTimeoutUnit
            // 
            this.lblProcessTimeoutUnit.AutoSize = true;
            this.lblProcessTimeoutUnit.Location = new System.Drawing.Point(251, 181);
            this.lblProcessTimeoutUnit.Name = "lblProcessTimeoutUnit";
            this.lblProcessTimeoutUnit.Size = new System.Drawing.Size(77, 12);
            this.lblProcessTimeoutUnit.TabIndex = 16;
            this.lblProcessTimeoutUnit.Text = "milliseconds";
            // 
            // ctlDebugSSL
            // 
            this.ctlDebugSSL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlDebugSSL.AutoSize = true;
            this.ctlDebugSSL.Location = new System.Drawing.Point(335, 181);
            this.ctlDebugSSL.Name = "ctlDebugSSL";
            this.ctlDebugSSL.Size = new System.Drawing.Size(120, 16);
            this.ctlDebugSSL.TabIndex = 18;
            this.ctlDebugSSL.Text = "Enable SSL Debug";
            this.ctlDebugSSL.UseVisualStyleBackColor = true;
            // 
            // grpProcessOutput
            // 
            this.grpProcessOutput.Controls.Add(this.ctlOutputText);
            this.grpProcessOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpProcessOutput.Location = new System.Drawing.Point(3, 218);
            this.grpProcessOutput.Name = "grpProcessOutput";
            this.grpProcessOutput.Padding = new System.Windows.Forms.Padding(0);
            this.grpProcessOutput.Size = new System.Drawing.Size(478, 117);
            this.grpProcessOutput.TabIndex = 1;
            this.grpProcessOutput.TabStop = false;
            this.grpProcessOutput.Text = "Output";
            // 
            // ctlOutputText
            // 
            this.ctlOutputText.BackColor = System.Drawing.SystemColors.Window;
            this.ctlOutputText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlOutputText.Location = new System.Drawing.Point(0, 14);
            this.ctlOutputText.Multiline = true;
            this.ctlOutputText.Name = "ctlOutputText";
            this.ctlOutputText.ReadOnly = true;
            this.ctlOutputText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ctlOutputText.Size = new System.Drawing.Size(478, 103);
            this.ctlOutputText.TabIndex = 0;
            // 
            // grpProcessError
            // 
            this.grpProcessError.Controls.Add(this.ctlErrorText);
            this.grpProcessError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpProcessError.Location = new System.Drawing.Point(3, 341);
            this.grpProcessError.Name = "grpProcessError";
            this.grpProcessError.Padding = new System.Windows.Forms.Padding(0);
            this.grpProcessError.Size = new System.Drawing.Size(478, 117);
            this.grpProcessError.TabIndex = 2;
            this.grpProcessError.TabStop = false;
            this.grpProcessError.Text = "Error";
            // 
            // ctlErrorText
            // 
            this.ctlErrorText.BackColor = System.Drawing.SystemColors.Window;
            this.ctlErrorText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlErrorText.ForeColor = System.Drawing.Color.Firebrick;
            this.ctlErrorText.Location = new System.Drawing.Point(0, 14);
            this.ctlErrorText.Multiline = true;
            this.ctlErrorText.Name = "ctlErrorText";
            this.ctlErrorText.ReadOnly = true;
            this.ctlErrorText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ctlErrorText.Size = new System.Drawing.Size(478, 103);
            this.ctlErrorText.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.ctlTablePanel);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Check ActiveMQ";
            this.ctlTablePanel.ResumeLayout(false);
            this.ctlInputPanel.ResumeLayout(false);
            this.ctlInputPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlCheckProcessTimeout)).EndInit();
            this.grpProcessOutput.ResumeLayout(false);
            this.grpProcessOutput.PerformLayout();
            this.grpProcessError.ResumeLayout(false);
            this.grpProcessError.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ctlTablePanel;
        private System.Windows.Forms.Panel ctlInputPanel;
        private System.Windows.Forms.Label lblOpenwireUri;
        private System.Windows.Forms.TextBox ctlOpenwireText;
        private System.Windows.Forms.TextBox ctlStompText;
        private System.Windows.Forms.Label lblStompUri;
        private System.Windows.Forms.TextBox ctlTopicText;
        private System.Windows.Forms.Label lblTopicName;
        private System.Windows.Forms.TextBox ctlBrokerUser;
        private System.Windows.Forms.Label lblBrokerUserName;
        private System.Windows.Forms.TextBox ctlBrokerPass;
        private System.Windows.Forms.Label lblBrokerPasscode;
        private System.Windows.Forms.TextBox ctlTrustStorePass;
        private System.Windows.Forms.Label lblTrustStorePassword;
        private System.Windows.Forms.TextBox ctlTrustStoreFile;
        private System.Windows.Forms.Label lblTrustStoreFullPath;
        private System.Windows.Forms.NumericUpDown ctlCheckProcessTimeout;
        private System.Windows.Forms.Label lblCheckProcessTimeout;
        private System.Windows.Forms.Label lblProcessTimeoutUnit;
        private System.Windows.Forms.Button ctlCheckButton;
        private System.Windows.Forms.GroupBox grpProcessOutput;
        private System.Windows.Forms.GroupBox grpProcessError;
        private System.Windows.Forms.TextBox ctlOutputText;
        private System.Windows.Forms.TextBox ctlErrorText;
        private System.Windows.Forms.CheckBox ctlDebugSSL;
    }
}

