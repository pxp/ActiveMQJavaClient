# ActiveMQJavaClient

### 项目介绍

使用 JDK 11 开发的 ActiveMQ 客户端示例


### 软件架构

`/MQCheckUI/CheckMQ/bin/Debug/cli/MQCheckTool.java` 是一个命令行程序。通过环境变量获取输入参数，使用 Stomp over WebSocket 协议接收消息，使用 OpenWire 协议发送消息。

`/MQCheckUI/CheckMQ/bin/Debug/CheckMQ.exe` 是 `MQCheckTool.java` 的前端，通过 .NET WinForm 来接收参数，并通过调用 JDK 11 来编译和运行 `MQCheckTool.java`。


### 安装教程

1. 克隆本 GIT 仓库至本地
2. 下载 [OpenJDK11](https://download.java.net/java/ga/jdk11/openjdk-11_windows-x64_bin.zip)
3. 将 JDK 11 中的 `bin`、`conf`、`include`、`jmods`、`legal`、`lib`、`release` 等目录和文件，复制到 `/MQCheckUI/CheckMQ/bin/Debug/jdk` 目录下


### 使用说明

如果要通过 WinForm 前端使用，请双击运行 `/MQCheckUI/CheckMQ/bin/Debug/CheckMQ.exe`

如果直接在命令行下编译执行 `MQCheckTool.java`，先将工作目录设为 `MQCheckTool.java` 所在目录（`/MQCheckUI/CheckMQ/bin/Debug/cli`）；然后设置环境变量；最后参考以下命令编译执行 Java 源码（假设 JDK 11 安装在 `X:\openjdk` 目录下）：

```bash
X:\openjdk\bin\java.exe -Dfile.encoding=UTF-8 -classpath ".;activemq-stomp-5.15.6.jar;activemq-client-5.15.6.jar;commons-io-2.6.jar;geronimo-jms_1.1_spec-1.1.jar;geronimo-j2ee-management_1.1_spec-1.0.1.jar;slf4j-api-1.7.25.jar;hawtbuf-1.11.jar;log4j-1.2.17.jar;slf4j-log4j12-1.7.25.jar" MQCheckTool.java
```

如果无需提供包含证书的 TrustStore，可以将 `MQCheckTool.java -> MQCheckTool -> main` 函数中的以下代码删除：

```java
if (websocketUri.startsWith("wss:")) {
    System.setProperty("javax.net.ssl.trustStore",
        envMap.getOrDefault("TRUST_KEYSTORE",
            TRUST_KEYSTORE.toAbsolutePath().normalize().toString())
    );
    System.setProperty("javax.net.ssl.trustStorePassword",
        envMap.getOrDefault("KEYSTORE_PASSWORD", KEYSTORE_PASSWORD)
    );
    System.setProperty("javax.net.ssl.trustStoreType",
        envMap.getOrDefault("KEYSTORE_TYPE", KEYSTORE_TYPE)
    );
}
```
