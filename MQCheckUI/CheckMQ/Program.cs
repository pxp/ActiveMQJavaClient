﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckMQ
{
    public static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        public static ProcessStartInfo AddEnvVar(this ProcessStartInfo info, string name, string value)
        {
            if (String.IsNullOrEmpty(value)) return info;

            info.EnvironmentVariables.Remove(name);
            info.EnvironmentVariables.Add(name, value);
            return info;
        }

        internal static string Directory
        {
            get
            {
                return Path.GetDirectoryName(typeof(Program).Assembly.Location);
            }
        }
    }
}
